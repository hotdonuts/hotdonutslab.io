var imgId = 'donut'
var offsetX = -250
var offsetY = -190


function mouseX(event) {
  var x = 200
  if (event.pageX) {
    x = event.pageX
  } else if (event.clientX) {
    x = event.clientX + (document.documentElement.scrollLeft ?  document.documentElement.scrollLeft : document.body.scrollLeft)
  }
  return x + offsetX + 'px'

}

function mouseY(event) {
  var y = 500
  if (event.pageY) {
    y = event.pageY
  } else if (event.clientY) {
    y = event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)
  }
  return y + offsetY + 'px'
}


function follow(event) {
  if (!event) {
    event = window.event
  }
  var obj = document.getElementById(imgId).style;
  var x = mouseX(event)
  var y = mouseY(event)
  obj.left = x
  obj.top = y
  console.log(x, y)
}

document.onmousemove = follow;
